// prisma/seed.ts
import { v4 as uuidv4 } from 'uuid';
import { PrismaClient } from './client';

const prisma = new PrismaClient()

async function main() {

  await prisma.userRole.create({
    data: {
      uuid: 'ADMIN', // Generate a random UUID
      name: 'ADMIN'
    },
  });

  await prisma.userRole.create({
    data: {
      uuid: 'USER', // Generate a random UUID
      name: 'USER'
    },
  });

  for (let i = 0; i < 20; i++) {
    await prisma.user.create({
      data: {
        uuid: uuidv4(), // Generate a random UUID
        username: `User${i + 1}`, // User1, User2, ..., User20
        password: '123SDs1',
        status: 'ACTIVE',
        userRoleId: 'ADMIN',
        createdBy: 'uuid1',
        updatedBy: 'uuid2',
      },
    });
  }
}

main()
  .then(async () => {
    await prisma.$disconnect()
  })
  .catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
  })
