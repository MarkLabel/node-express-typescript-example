import UserController from 'api/controllers/user.controller';
import { Routes } from 'api/interfaces/routes.interface';
import { Router } from 'express';

class UserRoute implements Routes {
  public path = '/user';
  public router = Router();
  public UserController = new UserController();

  constructor() {
    this.initializeRoutes();
  }
  private initializeRoutes() {
    //method get
    this.router.get(`${this.path}/all`, this.UserController.getAll); //เรียกข้อมูล User ทั้งหมด
    this.router.get(`${this.path}/find/:id`, this.UserController.getUserById); // เรียกข้อมูล User ด้วย UserId
    this.router.get(`${this.path}/mapData`, this.UserController.userMapData); // การจัดเรียงข้อมูลก่อน response
    this.router.get(`${this.path}/include`, this.UserController.userInclude); // ดึงข้อมูลอีกตาราง one to many

    //method post
    this.router.post(`${this.path}/pagination`, this.UserController.getUserPagination); //เรียกข้อมูล User ทั้งหมด ในรูปแบบ pagination สำหรับ data table
    this.router.post(`${this.path}/searchAndPagination`, this.UserController.getUserSearchAndPagination); //เรียกข้อมูล User ในรูปแบบ ส่งค่า filter การค้นหา และ pagination สำหรับ data table
    this.router.post(`${this.path}/create`, this.UserController.createUser); //สร้างข้อมูล User 1 user
    this.router.post(`${this.path}/createMultiple`, this.UserController.createUserMultiple); //สร้างข้อมูล User แบบมากกว่า 1 user พร้อมกันหลาย User

    //method put
    this.router.put(`${this.path}/update`, this.UserController.updateUser); //อัพเดทข้อมูล User 1 user
    this.router.put(`${this.path}/updateMultiple`, this.UserController.updateUserMultiple); //อัพเดทข้อมูล User แบบ array พร้อมกันหลาย User
    this.router.put(`${this.path}/delete`, this.UserController.deleteUser); //อัพเดทข้อมูล User แบบ array พร้อมกันหลาย User
  }
}

export default UserRoute;
