import { prisma } from 'libs/prisma';
import { isEmpty } from 'libs/utils/utils';
import { v4 as uuidv4 } from 'uuid';
import { deleteUserTypes, updateUserTypes, Filters, PaginationTypes, SearchPaginationTypes, createUserTypes } from 'api/interfaces/user.interface';
class userService {
  public getAllUsers = async () => {
    const result = await prisma.user.findMany({
      where: {
        status: 'ACTIVE',
      }
    })

    return result;
  };

  public getUserById = async (userId: string) => {

    const result = await prisma.user.findFirst({
      where: {
        uuid: userId
      }
    })

    if (isEmpty(result)) {
      throw new Error('User not found');
    }

    return result;
  };

  public mapUserData = async () => {
    const result = await prisma.user.findMany({
      where: {
        status: 'ACTIVE',
      }
    })

    const dataMap = result.map((value) => {
      return {
        userId: value.uuid,
        username: value.username,
        password: value.password
      }
    })

    return dataMap
  };

  public userInclude = async () => {
    //ทำงานโดย เอา userRoleId ที่อยุ่ใน table user ไปเชื่อมกับ uuid ของ userRole
    const result = await prisma.user.findMany({
      where: {
        status: 'ACTIVE',
      },
      include: {
        userRole: true
      }
    })

    return result
  };

  public getUserPagination = async (args: PaginationTypes) => {
    const count = await prisma.user.count({
      where: {
        status: 'ACTIVE',
      },
    })

    const result = await prisma.user.findMany({
      where: {
        status: 'ACTIVE',
      },
      take: args.limit || 10, //เรียกมาในจำนวนที่กำหนด เช่น ดึงมาแค่ 10 rows
      skip: args.limit * args.offset || 0, //เรียกข้อมูลลำดับที่ 0 หรือตั้งแต่ 10ขี้นไป
    })

    return {
      data: result,
      currentData: result.length,
      totalData: count,
    };
  };


  public getUserSearchAndPagination = async (args: SearchPaginationTypes) => {

    //ดึงจำนวนที่หาได้ทั้งหมด โดยไม่กำหนด take กับ skip
    const { offset = 0, limit = 10, userId, userRoleId, status } = args;

    // สร้างตัวกรองสำหรับการค้นหา
    const filters: Filters = {};

    //check search ค่่าของ การค้นหา status
    if (status) {
      filters.status = status;
    }

    //check search ค่่าของ การค้นหา userId
    if (userId) {
      filters.uuid = userId;
    }

    //check search ค่่าของ การค้นหา userRoleId
    if (userRoleId) {
      filters.userRoleId = userRoleId;
    }

    // ดึงจำนวนที่หาได้ทั้งหมด โดยไม่กำหนด take กับ skip
    const count = await prisma.user.count({
      where: filters,
    });

    // ดึงข้อมูลตามตัวกรองและแบ่งหน้า
    const result = await prisma.user.findMany({
      where: filters,
      take: limit,
      skip: limit * offset,
    });

    return {
      data: result,
      currentData: result.length,
      totalData: count,
    };
  };

  public createUser = async (args: createUserTypes) => {
    const result = await prisma.user.create({
      data: {
        uuid: uuidv4(),
        username: args.username,
        password: args.password,
        status: 'ACTIVE',
        userRoleId: args.userRoleId,
        createdBy: args.createdBy,
      }
    })

    return result;
  };

  public createMultiple = async (args: createUserTypes[]) => {
    const createdUsers = await Promise.all(args.map(user =>
      prisma.user.create({
        data: {
          uuid: uuidv4(),
          username: user.username,
          password: user.password,
          status: 'ACTIVE',
          userRoleId: user.userRoleId,
          createdBy: user.createdBy,
        },
      })
    ));

    return createdUsers;
  };

  public updateUser = async (args: updateUserTypes) => {
    const result = await prisma.user.update({
      where: {
        uuid: args.userId,
      },
      data: {
        username: args.username,
        password: args.password,
        userRoleId: args.userRoleId,
        updatedBy: args.updatedBy,
      }
    })

    return result;
  };

  public updateUserMultiple = async (args: updateUserTypes[]) => {
    const updatedUsers = await Promise.all(args.map(user =>
      prisma.user.update({
        where: {
          uuid: user.userId,
        },
        data: {
          username: user.username,
          password: user.password,
          userRoleId: user.userRoleId,
          updatedBy: user.updatedBy,
        }
      })
    ));

    return updatedUsers;
  };
  public deleteUser = async (args: deleteUserTypes) => {
    const result = await prisma.user.update({
      where: {
        uuid: args.userId,
      },
      data: {
        status: 'INACTIVE'
      }
    })
    return result;
  };



}

export default userService;
