import { NextFunction, Request, Response } from 'express';
import UserService from 'api/services/user.services';
import { handleZodError } from 'api/exceptions/zodError';
import { deleteUserSchema, updateUserMultipleSchema, updateUserSchema, getUserSearchAndPaginationSchema, getUserPaginationSchema, createUserSchema, createUserMultipleSchema } from 'api/schema/user';
class UserController {
  public userService = new UserService();

  public getAll = async (
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<void> => {
    try {
      const result = await this.userService.getAllUsers();
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  };

  public getUserById = async (
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<void> => {
    try {
      const { id } = req.params;
      const result = await this.userService.getUserById(id);
      res.status(200).json(result);
    } catch (error) {
      res.status(400).json({ message: error.message, status: 400 });
    }
  };

  public userMapData = async (
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<void> => {
    try {
      const mappedData = await this.userService.mapUserData();
      res.status(200).json(mappedData);
    } catch (error) {
      next(error);
    }
  };

  public userInclude = async (
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<void> => {
    try {
      const mappedData = await this.userService.userInclude();
      res.status(200).json(mappedData);
    } catch (error) {
      next(error);
    }
  };

  public getUserPagination = async (
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<void> => {
    try {
      getUserPaginationSchema.parse(req.body);
      const response = await this.userService.getUserPagination(req.body);
      res.status(200).send(response);
    } catch (error) {
      handleZodError(error, req, res, next);
    }
  };

  public getUserSearchAndPagination = async (
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<void> => {
    try {
      getUserSearchAndPaginationSchema.parse(req.body);
      const response = await this.userService.getUserSearchAndPagination(req.body);
      res.status(200).send(response);
    } catch (error) {
      handleZodError(error, req, res, next);
    }
  };

  public createUser = async (
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<void> => {
    try {
      createUserSchema.parse(req.body);
      const response = await this.userService.createUser(req.body);
      res.status(200).send(response);
    } catch (error) {
      handleZodError(error, req, res, next);
    }
  };

  public createUserMultiple = async (
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<void> => {
    try {
      createUserMultipleSchema.parse(req.body);
      const response = await this.userService.createMultiple(req.body);
      res.status(200).send(response);
    } catch (error) {
      handleZodError(error, req, res, next);
    }
  };

  public updateUser = async (
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<void> => {
    try {
      updateUserSchema.parse(req.body);
      const response = await this.userService.updateUser(req.body);
      res.status(200).send(response);
    } catch (error) {
      handleZodError(error, req, res, next);
    }
  };

  public updateUserMultiple = async (
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<void> => {
    try {
      updateUserMultipleSchema.parse(req.body)
      const response = await this.userService.updateUserMultiple(req.body);
      res.status(200).send(response);
    } catch (error) {
      handleZodError(error, req, res, next);
    }
  };

  public deleteUser = async (
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<void> => {
    try {
      deleteUserSchema.parse(req.body)
      const response = await this.userService.deleteUser(req.body);
      res.status(200).send(response);
    } catch (error) {
      handleZodError(error, req, res, next);
    }
  };
}

export default UserController;
