import { ZodError } from 'zod';
import { Request, Response, NextFunction } from 'express';

export const handleZodError = (
  error: any,
  req: Request,
  res: Response,
  next: NextFunction
): void => {
  if (error instanceof ZodError) {
    const errors = error.errors.map((err) => ({
      code: err.code,
      field: err.path.join('.'),
      message: err.message,
    }));
    res.status(400).json({ errors }); // response ข้อผิดพลาดในรูปแบบ JSON
  } else {
    next(error); // กรณี error อื่นๆ ส่งไปยัง middleware ถัดไป
  }
};
