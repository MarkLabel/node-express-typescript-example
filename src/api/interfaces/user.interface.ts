export interface Filters {
  status?: string;
  uuid?: string;
  userRoleId?: string;

}

export interface PaginationTypes {
  limit: number;
  offset?: number;
}


export interface SearchPaginationTypes {
  limit: number;
  offset?: number;
  status?: string;
  userId?: string;
  userRoleId?: string;
}

export interface SearchPaginationTypes {
  limit: number;
  offset?: number;
  status?: string;
  userId?: string;
  userRoleId?: string;
}


export interface createUserTypes {
  username: string;
  password: string;
  userRoleId: string;
  createdBy: string;
}


export interface updateUserTypes {
  username: string;
  password: string;
  userRoleId: string;
  createdBy: string;
  updatedBy: string
  userId: string
}


export interface deleteUserTypes {
  userId: string
}
