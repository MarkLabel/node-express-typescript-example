import { z } from 'zod'

export const getUserSearchAndPaginationSchema = z.object({
  limit: z.number(),
  offset: z.number(),
  userId: z.string(),
  userIdRole: z.string(),
  status: z.string(),
})

export const getUserPaginationSchema = z.object({
  limit: z.number(),
  offset: z.number(),
})

export const createUserSchema = z.object({
  username: z.string(),
  password: z.string(),
  userRoleId: z.string(),
  createdBy: z.string(),
})

export const createUserMultipleSchema = z.array(createUserSchema);

export const updateUserSchema = z.object({
  userId: z.string(),
  username: z.string(),
  password: z.string(),
  userRoleId: z.string(),
  updatedBy: z.string(),
})

export const updateUserMultipleSchema = z.array(updateUserSchema);

export const deleteUserSchema = z.object({
  userId: z.string(),
})

